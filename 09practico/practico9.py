import cv2
import numpy as np


class Measure:
    def __init__(self) -> None:
        self.print_menu()

        self.img_name = "libro1.png"
        self.img = cv2.imread(self.img_name)

        self.draw = False
        self.points = []
        self.x0, self.y0 = 0, 0

        # Medidas de referencia en cm
        self.W = 15
        self.H = 22

        # Puntos de la rectificacion
        self.points = [[25, 43], [655, 69], [90, 938], [600, 888]]
        self.points = self.ordena_puntos()
        self.scale = 41

        # Transformacion
        self.img = self.rectificar()
        self.img_aux = self.img.copy()
        self.img_rec_med = []
        cv2.imwrite("transform.jpg", self.img)

        while True:
            cv2.imshow("imagen", self.img)
            key = cv2.waitKey(1)

            if key == ord("q"):
                break
            elif key == ord("r"):
                self.img = self.img_aux.copy()
                self.img_rec_med = self.img.copy()
            elif key == ord("g"):
                cv2.imwrite("measure.jpg", self.img)
            elif key == ord("h"):
                self.img_rec_med = self.img.copy()
                cv2.setMouseCallback("imagen", self.measurement)

        cv2.destroyAllWindows()

    def print_menu(self):
        print("Se seleccionó 4 puntos en la imagen para rectificar")
        print("Con la letra 'h'se procedera a medir la imagen")
        print("Se debe hacer click y mantener apretado para medir")
        print("Con la letra 'g' se guarda la imagen con los puntos medidos")
        print("Con la letra 'r' se reinicia la medición")
        print("Con la letra 'q' finaliza la ejecución del programa")

    def ordena_puntos(self) -> list:
        points = self.points
        mod = []
        # Calcula modulos
        for x, y in points:
            mod.append(np.sqrt(x**2 + y**2))

        # Arreglo de puntos ordenados
        new_points = [0, 0, 0, 0]

        # Define puntos A y D
        new_points[0] = points.pop(mod.index(min(mod)))
        new_points[3] = points.pop(mod.index(max(mod)) - 1)

        # Define puntos B y C
        if points[0][1] < points[1][1]:
            new_points[1] = points[0]
            new_points[2] = points[1]
        else:
            new_points[1] = points[1]
            new_points[2] = points[0]

        return new_points

    def rectificar(self) -> np.ndarray:
        # p_origen [A,B,C,D]
        p_origen = np.float32(self.points)

        # Diferencia de puntos BA, DC, CA y DB
        dif_BA = p_origen[1] - p_origen[0]
        dif_DC = p_origen[3] - p_origen[2]
        dif_CA = p_origen[2] - p_origen[0]
        dif_DB = p_origen[3] - p_origen[1]

        # Ancho y largo
        w_AB = np.sqrt(dif_BA[0] ** 2 + dif_BA[1] ** 2)
        w_CD = np.sqrt(dif_DC[0] ** 2 + dif_DC[1] ** 2)
        h_AC = np.sqrt(dif_CA[0] ** 2 + dif_CA[1] ** 2)
        h_BD = np.sqrt(dif_DB[0] ** 2 + dif_DB[1] ** 2)

        # Ancho y largo maximos
        wide = max(int(w_AB), int(w_CD)) - 1
        height = max(int(h_BD), int(h_AC)) - 1

        # Puntos destino
        p_destino = np.float32([[0, 0], [wide, 0], [0, height], [wide, height]])
        M = cv2.getPerspectiveTransform(p_origen, p_destino)
        img_salida = cv2.warpPerspective(self.img, M, (wide, height))
        return img_salida

    def measurement(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.x0, self.y0 = x, y
            self.draw = True
        elif event == cv2.EVENT_MOUSEMOVE and self.draw is True:
            self.img = self.img_rec_med.copy()
            cv2.circle(self.img, (self.x0, self.y0), 5, (100, 255, 0), -1)
            cv2.circle(self.img, (x, y), 5, (100, 255, 0), -1)
            cv2.line(self.img, (self.x0, self.y0), (x, y), (145, 255, 140), 2)
            distance = (np.sqrt((self.x0 - x) ** 2 + (self.y0 - y) ** 2)) / 100
            text_position = int((self.x0 + x) / 2 + 15), int((self.y0 + y) / 2 - 15)
            cv2.putText(
                self.img,
                "{:.2f}[m]".format(distance / self.scale),
                (text_position),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.8,
                (0, 0, 255),
                1,
            )
        elif event == cv2.EVENT_LBUTTONUP:
            self.img_rec_med = self.img.copy()
            self.draw = False


if __name__ == "__main__":
    practico = Measure()
