import cv2  # Importamos libreria para leer imagen, obtener tamaño, transformarla y mostrarla
import numpy as np  # Importamos libreria para funciones trigonometricas y arreglos


class TransformacionAfin:
    def __init__(self) -> None:
        self.img = cv2.imread("messi.png")  # Cargamos la imagen
        (self.h, self.w, self.d) = self.img.shape  # Obtenemos el tamaño
        # En particular para este práctico, se decidió arbitrariamente aplicar un espejado horizontal y vertical

        # Se eligen como puntos originales la esquina superior izquierda, la esquina superior derecha y la esquina inferior izquierda
        self.puntos_orig = np.float32([[0, 0], [self.w - 1, 0], [0, self.h - 1]])
        # Al realizar un espejado tanto vertical como horizontal,
        self.puntos_transf = np.float32(
            [[self.w - 1, self.h - 1], [0, self.h - 1], [self.w - 1, 0]]
        )
        # el punto correspondiente a la esquina superior izquierda, es la esquina inferior derecha;
        # las otras 2 esquinas se intercambian entre sí

        self.imagen = self.transf_afin()

        # Mostramos la imagen transformada
        cv2.imshow("Imagen transformada", self.imagen)
        cv2.waitKey(5000)  # Esperamos 5 segundos

        cv2.destroyAllWindows()  # Cerramos todas las ventanas abiertas

    def transf_afin(self) -> list:

        M = cv2.getAffineTransform(self.puntos_orig, self.puntos_transf)
        # Aplicamos la transformacion
        img_transf = cv2.warpAffine(self.img, M, (self.w, self.h))

        # Devolvemos la imagen transformada(h, w, d) = self.img.shape   #Obtenemos el tamaño
        return img_transf


if __name__ == "__main__":
    practico = TransformacionAfin()
