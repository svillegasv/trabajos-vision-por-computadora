import cv2
import numpy as np


class InsertarImg:
    def __init__(self) -> None:
        self.img = cv2.imread("messi.png")
        self.img_aux = cv2.imread("messi.png")

        self.points = []

        cv2.namedWindow("imagen")
        cv2.setMouseCallback("imagen", self.draw_points)

        self.print_menu()

        while True:
            cv2.imshow("imagen", self.img)
            key = cv2.waitKey(1)

            if key == ord("q"):
                break
            elif key == ord("r"):
                self.img = cv2.imread("messi.png")
            elif key == ord("a"):
                if len(self.points) < 3:
                    print("\nDebe seleccionar 3 puntos primero")
                else:
                    self.img = self.img_aux.copy()
                    self.imgx = cv2.imread("copa_america.jpg")
                    self.img = self.insertar()
                    cv2.imwrite("output.jpg", self.img)

        cv2.destroyAllWindows()

    def print_menu(self) -> None:
        print("Con el mouse puede seleccionar los puntos donde insertar")
        print("Con la letra 'a' inserta una imagen en la otra")
        print("Con la letra 'q' finaliza la ejecucion del programa")

    def draw_points(self, event, x, y, flags, param) -> None:
        if event == cv2.EVENT_LBUTTONDOWN:
            if len(self.points) < 3:
                self.points.append([x, y])
            else:
                self.points.clear()
                self.points.append([x, y])
                self.img = self.img_aux.copy()
        elif event == cv2.EVENT_LBUTTONUP:
            cv2.circle(self.img, (x, y), 5, (0, 0, 255), 2)

    def insertar(self) -> list:
        # Hago transformacion en imgx
        cols, rows, ch = self.imgx.shape
        pt1 = np.float32([[0, 0], [rows - 1, 0], [0, cols - 1]])
        pt2 = np.float32(self.points)

        M = cv2.getAffineTransform(pt1, pt2)
        imga = cv2.warpAffine(self.imgx, M, (rows, cols))

        # Hago un hueco en img
        mascara = np.array([pt2[1], pt2[0], pt2[2], pt2[2] + pt2[1] - pt2[0]], np.int32)

        cv2.fillPoly(self.img, [mascara], (0, 0, 0), cv2.LINE_AA)

        # Llevo imgx al tamaño de img
        cols, rows, ch = self.img.shape
        imga = imga[0:cols, 0:rows]

        # Sumo imagenes
        self.img = cv2.add(self.img, imga)
        return self.img


if __name__ == "__main__":
    practico = InsertarImg()
